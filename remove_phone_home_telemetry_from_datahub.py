import sys
import re

from pathlib import Path

site_packages_dir = Path(sys.argv[1])
datahub_airflow_plugin_dir = site_packages_dir / 'datahub_airflow_plugin'
tainted_files = ('datahub_listener.py', 'datahub_plugin_v22.py')


def remove_method_call(file_path, method_name):
    try:
        # Read the entire content of the file
        with open(file_path, 'r') as file:
            content = file.read()

        # Define the regex pattern
        pattern = rf'{method_name}\s*\([^)]*\)'

        # Replace all occurrences of the matched pattern with an empty string
        modified_content = re.sub(pattern, '', content, re.MULTILINE)

        # Write the modified content back to the file
        with open(file_path, 'w') as f:
           f.write(modified_content)

        print(f"Successfully removed '{method_name}' calls from {file_path}")
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")


def main():
    for filename in tainted_files:
        remove_method_call(datahub_airflow_plugin_dir / filename, 'telemetry.telemetry_instance.ping')


if __name__ == "__main__":
    main()
