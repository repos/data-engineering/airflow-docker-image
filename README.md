# airflow-docker-image

This repository contains the build pipeline for the Data Science and Engineering deployments of [Apache Airflow](https://airflow.apache.org/) at the [Wikimedia Foundation](https://wikimediafoundation.org/).
